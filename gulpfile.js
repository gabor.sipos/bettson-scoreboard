var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    del = require('del'),
    sass = require('gulp-sass'),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    ngAnnotate = require('browserify-ngannotate');

var CacheBuster = require('gulp-cachebust');
var cachebust = new CacheBuster();

// Configures build paths
var config = {
    path: {
        bower_json:         './bower.json',
        scss:               './resources/assets/*.scss',
        css_sourcemap:      './',
        dist_css:           './dist/css',
        dist_js:            './dist/js',
        views:              './resources/views/*.html',
        templateCache:      'templateCache.js',
        jshint:             '/app/*.js',
        js_sourcemap:       './',
        browserify_entries: './app/app.js',
        browserify_paths:   ['./app/controllers', './app/services', './app/directives'],
        browserify_source:  'bundle.js',
        browserify_dest:    './dist/js/',
        index:              'index.html',
        build_dist:         'dist',
        watch_files:        ['./index.html','./resources/views/*.html', './resources/assets/*.*css', './app/**/*.js']
    },
    template: {
        module_name: "scoreboardTemplates",
        prefix: "/resources/views/"
    }
}

// Cleans build
gulp.task('clean', function (done, cb) {
    del([
        'dist'
    ], cb);
    done();
});

// Installs bower dependencies
gulp.task('bower', function() {

    var install = require("gulp-install");

    return gulp.src([config.path.bower_json])
        .pipe(install());
});

// Builds Styles
gulp.task('build-css', ['clean'], function() {
    return gulp.src(config.path.scss)
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.write(config.path.css_sourcemap))
        .pipe(gulp.dest(config.path.dist_css));
});

// Builds angular template cache
gulp.task('build-template-cache', ['clean'], function() {
    
    var ngHtml2Js = require("gulp-ng-html2js"),
        concat = require("gulp-concat");
    
    return gulp.src(config.path.views)
        .pipe(ngHtml2Js({
            moduleName: config.template.module_name,
            prefix: config.template.prefix
        }))
        .pipe(concat(config.path.templateCache))
        .pipe(gulp.dest(config.path.dist_js));
});

// Sysntax checking
gulp.task('jshint', function(done) {
    gulp.src(config.path.jshint)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
    done();
});

// Builds JavaScript
gulp.task('build-js', ['clean'], function() {
    var b = browserify({
        entries: config.path.browserify_entries,
        debug: true,
        paths: config.path.browserify_paths,
        transform: [ngAnnotate]
    });

    return b.bundle()
        .pipe(source(config.path.browserify_source))
        .pipe(buffer())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write(config.path.js_sourcemap))
        .pipe(gulp.dest(config.path.browserify_dest));
});

// Build all
gulp.task('build', ['clean', 'bower', 'build-css', 'build-template-cache', 'jshint', 'build-js'], function() {
    return gulp.src(config.path.index)
        .pipe(cachebust.references())
        .pipe(gulp.dest(config.path.build_dist));
});

// Builds with watch
gulp.task('watch', function() {
    return gulp.watch(config.path.watch_files, ['build']);
});

// Gulp server
gulp.task('webserver', ['watch','build'], function(done) {
    gulp.src('.')
        .pipe(webserver({
            livereload: false,
            directoryListing: true,
            open: "http://localhost:8000/dist/index.html"
        }));
    done();
});

// Builds with development environment
gulp.task('dev', ['watch', 'webserver']);

gulp.task('default', ['build']);