angular = require('angular');

angular.module('bettson-scoreboard')
	.factory('scoreboardStorage', function ($http, $injector) {
		'use strict';
		return $injector.get('localStorage');
	})
	.factory('localStorage', function ($q) {
		'use strict';

		var STORAGE_ID = 'bettson-scoreboard-storage';

		var store = {
			scoreboard: undefined,

			_getFromLocalStorage: function () {
				var storage = window.localStorage[STORAGE_ID];
				if(storage !== undefined) {
					this.scoreboard = JSON.parse(storage);
					
				}
				return this.scoreboard;
			},	

			_saveToLocalStorage: function (newElement) {
				this.scoreboard = newElement;
				window.localStorage[STORAGE_ID] = JSON.stringify(this.scoreboard);
			},

			delete: function (element) {
				store.scoreboard.splice(store.scoreboard.indexOf(element), 1);
				window.localStorage[STORAGE_ID] = JSON.stringify(this.scoreboard);
			},

			get: function () {
			},

			insert: function (element) {
				store.scoreboard.push(element);
				window.localStorage[STORAGE_ID] = JSON.stringify(this.scoreboard);
			},

			put: function (element, index) {
				store.scoreboard[index] = element;
				window.localStorage[STORAGE_ID] = JSON.stringify(this.scoreboard);
			}
		};

		return store;
	});
