angular = require('angular');
require('angular-route');
require('../dist/js/templateCache');


angular.module('bettson-scoreboard', ['ngRoute','scoreboardTemplates'])
	.config(function ($routeProvider) {
		'use strict';

		var routeConfig = {
			controller: 'ScoreboardController',
			templateUrl: '/resources/views/scoreboard.html',
			resolve: {
				store: ['scoreboardStorage', function (storage) {
					return storage;
				}]
			}
		};

		$routeProvider
			.when('/', routeConfig)
			.otherwise({
				redirectTo: '/'
			});
	});

require('scoreboardController');
require('scoreboardStorage');