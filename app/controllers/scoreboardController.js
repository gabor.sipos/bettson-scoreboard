angular = require('angular');

angular.module('bettson-scoreboard')
	.controller('ScoreboardController', function ScoreboardController($scope, $routeParams, $filter, store) {
		'use strict';

		$scope.scoreboard = store._getFromLocalStorage() || {
			timer: {
				match_length: 90,
				period: '1st',
				isRunning: false,
				current_time: '00:00',
				speed: 0.1,
				minutes: 0,
				seconds: 0
			},
			stats: {
				goals: {
					home: 0,
					away: 0
				},
				corners: {
					home: 0,
					away: 0
				},
				yellow_cards: {
					home: 0,
					away: 0
				},
				red_cards: {
					home: 0,
					away: 0
				}
			},
			results: {
				first_half: {
					home: 0,
					away: 0
				},
				second_half: {
					home: 0,
					away: 0
				}
			}
		}

		$scope.increaseTimer = function () {
			var timer = $scope.scoreboard.timer;
			timer.seconds++;

			if(timer.seconds > 60) {
				timer.minutes++;
				timer.seconds = 0;
			}

			if(timer.seconds < 10) {
				var sStr = '0' + timer.seconds.toString();
			} else {
				var sStr = timer.seconds.toString();
			}

			if(timer.minutes < 10) {
				var mStr = '0' + timer.minutes.toString();
			} else {
				var mStr = timer.minutes.toString();
			}

			timer.current_time = mStr.concat(':', sStr);

			setTimeout(function() {
				if($scope.scoreboard.timer.isRunning) {
					$scope.$apply(function () {
						if(timer.minutes >= timer.match_length) {
							$scope.scoreboard.results.second_half.home = $scope.scoreboard.stats.goals.home;
							$scope.scoreboard.results.second_half.away = $scope.scoreboard.stats.goals.away;
							$scope.saveToStorage();
							$scope.stopTimer();
						} 
						else if(timer.minutes == timer.match_length / 2 && timer.seconds == 0) {
							$scope.setPeriod(2);
							$scope.scoreboard.results.first_half.home = $scope.scoreboard.stats.goals.home;
							$scope.scoreboard.results.first_half.away = $scope.scoreboard.stats.goals.away;
							$scope.saveToStorage();
							$scope.stopTimer();
						} 
						else {
							$scope.saveToStorage();
							$scope.increaseTimer();
						}
					});
				}
			},$scope.scoreboard.timer.speed);
		}

		$scope.setPeriod = function (period) {
			$scope.scoreboard.timer.period = period.toString();
			if(period == 1)
				$scope.scoreboard.timer.period += 'st';
			else if(period == 2)
				$scope.scoreboard.timer.period += 'nd';
		}


		$scope.startTimer = function () {
			var timer = $scope.scoreboard.timer;
			if(timer.minutes < timer.match_length) {
				$scope.scoreboard.timer.isRunning = true;
				$scope.increaseTimer();
			}
		};

		$scope.stopTimer = function () {
			$scope.scoreboard.timer.isRunning = false;
		};

		$scope.resetTimer = function () {
			var timer = $scope.scoreboard.timer;
			var stats = $scope.scoreboard.stats;
			var results = $scope.scoreboard.results;
			
			timer.isRunning = false;
			timer.current_time = '00:00';
			timer.minutes = 0;
			timer.seconds = 0;

			stats.goals.home = 0;
			stats.goals.away = 0;

			stats.corners.home = 0;
			stats.corners.away = 0;

			stats.yellow_cards.home = 0;
			stats.yellow_cards.away = 0;

			stats.red_cards.home = 0;
			stats.red_cards.away = 0;

			results.first_half.home = 0;
			results.first_half.away = 0;

			results.second_half.home = 0;
			results.second_half.away = 0;

			$scope.setPeriod(1);
			$scope.saveToStorage();
		};

		$scope.setStat = function(team, prop, dir) {
			var stats = $scope.scoreboard.stats;

			switch(prop) {
				case 'goal':
					if(dir == 'plus')
						stats.goals[team] += 1;
					else if (dir == 'minus' && stats.goals[team] > 0)
						stats.goals[team] -= 1;
				break;
				case 'corner':
					if(dir == 'plus')
						stats.corners[team] += 1;
					else if (dir == 'minus' && stats.corners[team] > 0)
						stats.corners[team] -= 1;
				break;
				case 'yellow-card':
					if(dir == 'plus')
						stats.yellow_cards[team] += 1;
					else if (dir == 'minus' && stats.yellow_cards[team] > 0)
						stats.yellow_cards[team] -= 1;
				break;
				case 'red-card':
					if(dir == 'plus')
						stats.red_cards[team] += 1;
					else if (dir == 'minus' && stats.red_cards[team] > 0)
						stats.red_cards[team] -= 1;
				break;
			}
		}

		$scope.saveToStorage = function () {
			store._saveToLocalStorage($scope.scoreboard);
		}
	});
