# Installation instructions

## System requirements

- nodejs v8.9.4+
- npm 5.6.0+

## Checking out base repository

`~$ git clone http://git.gaborsipos.com/gabor.sipos/bettson-scoreboard.git`

## Installing global dependencies

- For linux:

`~$ npm run install-linux` 

or manually 

```
~$ npm install -g gulp
~$ npm install -g bower
```

- For Windows:

`~$ npm run install-windows`

or manually

```
~$ npm install -g windows-build-tools
~$ npm install -g gulp
~$ npm install -g bower
```

## Installing app dependencies

`~$ npm install`

## Compiling for production

`~$ npm run prod`

or manually

```
~$ gulp
```


## Compiling for development

`~$ npm run dev`

or manually

```
~$ gulp
~$ gulp dev
```
    
For development purposes it will start a local webserver. It will opens automatically in your default browser, but you can reach on the following url:
[http://localhost:8000/dist/index.html](http://localhost:8000/dist/index.html)

## Troubleshooting

- If something fails with `npm run` shortcuts, please use the manual methods!
- If `gulp dev` fails on using templateCache, just clean the whole dist directory with `gulp clean` and run it again!


# Usage of the application

## Layout & Control Behavior
There are two panels on the page, a scoreboard and a control panel.
Users can control only the statistics with `+` and `-` buttons, to prevent accidentally submitted input.
This application also uses `localStorage` to prevent dataloss from any browser error. If something happens the current state will be loaded automatically from localStorage after the reload of the page.
Users can reset the timer and statistics with `resetTimer` button. For example if a new match will start.

## Presentation
For the presentation I set the timer delay to 1 milisec to demostrate the behavior of the application. It is configurable in the `scoreboardController.js` with `speed` property of the `$scope.scoreboard.timer` variable. (In a real life match, it should be 1000ms)

## Timer behavior
This application's timer is stopping if it reaches the end of the first half period. Users can start the second half period by clicking again on the start button. This button will show what period will be started. At the end of the match the timer stops again and there are no more possibility to start it again, users have to reset the timer. The match has end. (In phase 2 time extension should be implemented based on the original role)

If the timer reaches the end of the first and second half period, it will set the results of the first and second half period automatically.

## Match length
The match's length is also configurable in the `scoreboardController.js` with `match_length` property.

